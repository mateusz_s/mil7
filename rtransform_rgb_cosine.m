function [Rt, Gt, Bt] = rtransform_rgb_cosine(R, G, B)
    Rt = uint8(idct2(R));
    Gt = uint8(idct2(G));
    Bt = uint8(idct2(B));
end

