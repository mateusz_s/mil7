image = load_picture('Leopard-with-noise.jpg');
image_clean = load_picture('Leopard.jpg');
figure(1);
imshow(image);
[R, G, B] = split_colors(image);
[Rt, Gt, Bt] = transform_rgb_cosine(R, G, B);

bestErr = Inf;
bestT = 0;
bestImage = image;

for t = 54:.1:56
    t
    [Rtt, Gtt, Btt] = soft_threshold_rgb(Rt, Gt, Bt, t);
    [Rtt, Gtt, Btt] = rtransform_rgb_cosine(Rtt, Gtt, Btt);
    image = rgb_to_img(Rtt, Gtt, Btt);
    N = length(image(1,:,:)) * length(image(:,1,:)) * length(image(:,:,1));
    err = sum(sum(sum((image_clean-image).^2)))/N;
    im2 = image_clean-image;
    if err < bestErr
        bestErr = err;
        bestT = t;
        bestImage = image;
    end
end

figure(2);
imshow(image);
bestT

figure(3);
imshow(bestImage);