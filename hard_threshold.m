function image = hard_threshold(image, t)
    image(abs(image) < t) = 0;
end

