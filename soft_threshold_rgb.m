function [Rt, Gt, Bt] = soft_threshold_rgb(R, G, B, t)
    Rt = wthresh(R,'s',t);
    Gt = wthresh(G,'s',t);
    Bt = wthresh(B,'s',t);
end

