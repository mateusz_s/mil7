function [R, G, B] = split_colors(image)
    R = image(:,:,1);
    G = image(:,:,2);
    B = image(:,:,3);
end