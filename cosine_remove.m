function err = cosine_remove(threshold, soft)
    image = load_picture('Leopard-with-noise.jpg');
    image_clean = load_picture('Leopard.jpg');
    figure(1);
    imshow(image);
    [R, G, B] = split_colors(image);
    [Rt, Gt, Bt] = transform_rgb_cosine(R, G, B);

    if soft
        [Rtt, Gtt, Btt] = soft_threshold_rgb(Rt, Gt, Bt, threshold);
    else
        [Rtt, Gtt, Btt] = hard_threshold_rgb(Rt, Gt, Bt, threshold);
    end
    [Rtt, Gtt, Btt] = rtransform_rgb_cosine(Rtt, Gtt, Btt);
    image = rgb_to_img(Rtt, Gtt, Btt);
    
    N = length(image(1,:,:)) * length(image(:,1,:)) * length(image(:,:,1));
    err = sum(sum(sum((image_clean-image).^2)))/N;
    im2 = image_clean-image;

    figure(2);
    imshow(image);
    
    figure(3);
    imshow(im2);
end

