function [Rt, Gt, Bt] = transform_rgb_cosine(R, G, B)
    Rt = dct2(R);
    Gt = dct2(G);
    Bt = dct2(B);
end

