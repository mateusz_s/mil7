function [Rt, Gt, Bt] = rwalsh_transform_rgb(R, G, B)
    Rt = uint8(ifwht(R));
    Gt = uint8(ifwht(G));
    Bt = uint8(ifwht(B));
end

