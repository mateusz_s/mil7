function [Rt, Gt, Bt] = walsh_transform_rgb(R, G, B)
    Rt = fwht(double(R));
    Gt = fwht(double(G));
    Bt = fwht(double(B));
end

