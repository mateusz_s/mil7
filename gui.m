function varargout = gui(varargin)
% GUI MATLAB code for gui.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui

% Last Modified by GUIDE v2.5 23-May-2017 17:56:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui (see VARARGIN)

% Choose default command line output for gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function alfa_slider_Callback(hObject, eventdata, handles)
    set(handles.alfa_value, 'String', num2str(get(hObject, 'Value')));
% hObject    handle to alfa_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function alfa_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alfa_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in remove_noise.
function remove_noise_Callback(hObject, eventdata, handles)
    str = get(handles.transform_type, 'String');
    val = get(handles.transform_type, 'Value');
    threshold = get(handles.alfa_slider, 'Value');
    soft = false;
    if get(handles.soft_button, 'Value') ~= 0
        soft = true;
    end
    % Set current data to the selected data set.
    err = 0;
    switch str{val}; 
    case 'Cosine' % User selects peaks.
        err = cosine_remove(threshold, soft);
    case 'Walsh-Hadamard' % User selects membrane.
        err = rwalsh_remove(threshold, soft);
    end
    set(handles.error_value, 'String', num2str(err));


% --- Executes on selection change in transform_type.
function transform_type_Callback(hObject, eventdata, handles)
% hObject    handle to transform_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns transform_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from transform_type
    str = get(hObject, 'String');
    val = get(hObject, 'Value');
    % Set current data to the selected data set.
    switch str{val};
    case 'Cosine' % User selects peaks.
        set(handles.alfa_slider, 'Value', 0);
        set(handles.alfa_value, 'String', '0');
        set(handles.alfa_slider, 'Max', 200);
    case 'Walsh-Hadamard' % User selects membrane.
        set(handles.alfa_slider, 'Value', 0);
        set(handles.alfa_value, 'String', '0');
        set(handles.alfa_slider, 'Max', 5);
    end

% --- Executes during object creation, after setting all properties.
function transform_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to transform_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over alfa_slider.
function alfa_slider_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to alfa_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in soft_button.
function soft_button_Callback(hObject, eventdata, handles)
    set(handles.hard_button, 'Value', 0);
% hObject    handle to soft_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of soft_button


% --- Executes on button press in hard_button.
function hard_button_Callback(hObject, eventdata, handles)
    set(handles.soft_button, 'Value', 0);
% hObject    handle to hard_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of hard_button
