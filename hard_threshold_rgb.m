function [Rt, Gt, Bt] = hard_threshold_rgb(R, G, B, t)
    Rt = hard_threshold(R, t);
    Gt = hard_threshold(G, t);
    Bt = hard_threshold(B, t);
end

